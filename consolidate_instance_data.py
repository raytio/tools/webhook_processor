# coding=utf-8
from datetime import datetime, timezone
import requests
import json
import os
import logging
import boto3
import botocore.exceptions
from typing import Tuple


API_ENV = os.environ.get("API_ENV", "dev")
API_URL = os.environ.get("API_URL", "https://api-dev.rayt.io")
CONSOLIDATE_START = os.environ.get("CONSOLIDATE_START", "2000-01-01T00:00:00+00:00")
# try:
#     CONSOLIDATE_START = datetime.fromisoformat(CONSOLIDATE_START)
# except:
#     pass
LOGGER = logging.getLogger("raytio.consolidate_instance_data")
if API_ENV == "prod":
    LOGGER.setLevel(logging.INFO)
else:
    LOGGER.setLevel(logging.DEBUG)


def lambda_handler(event, context):
    S3_BUCKET = load_parameters()
    # s3_bucket_objects = list_bucket_objects(S3_BUCKET)
    # Check for the summary file. This contains the date last processed
    processed_object_name = "consolidated/processed.summary"
    processed_object = get_s3_object_content(S3_BUCKET, processed_object_name)
    LOGGER.debug(f"processed_object = {processed_object}")
    if processed_object not in ("EMPTY", "NOT_FOUND"):
        date_last_processed_str = processed_object.get("date_last_processed")
        LOGGER.debug(f"date_last_processed_str = {date_last_processed_str}")
    else:
        date_last_processed_str = CONSOLIDATE_START
        LOGGER.debug(f"date_last_processed_str = {date_last_processed_str}")
        processed_object = {}
    try:
        date_last_processed = datetime.fromisoformat(date_last_processed_str)
    except (TypeError, ValueError):
        date_last_processed_str = CONSOLIDATE_START
        date_last_processed = datetime.fromisoformat(date_last_processed_str)

    LOGGER.debug(
        f"date_last_processed = {date_last_processed} and type = {type(date_last_processed)}"
    )
    current_date = datetime.now(timezone.utc).isoformat()
    # for s3_bucket_object in s3_bucket_objects:
    #     print(s3_bucket_object.key)

    # Load the existing consolidated json file
    consolidated_list_object_name = "consolidated/consolidated.json"
    consolidated_csvlist_object_name = "consolidated/consolidated.csv"
    consolidated_keys_list_object_name = "consolidated/consolidated_i_ids.json"
    consolidated_keys_csvlist_object_name = "consolidated/consolidated_i_ids.csv"
    consolidated_list = get_s3_object_content(S3_BUCKET, consolidated_list_object_name)
    if consolidated_list in ("EMPTY", "NOT_FOUND"):
        consolidated_list = []
    consolidated_keys_list = []
    # consolidated_keys_list = get_s3_object_content(
    #     S3_BUCKET, consolidated_keys_list_object_name
    # )
    # LOGGER.debug(f"consolidated_keys_list = {consolidated_keys_list}")
    # if consolidated_keys_list in ("EMPTY", "NOT_FOUND"):
    #     consolidated_keys_list = []
    #     LOGGER.debug(f"consolidated_keys_list = {consolidated_keys_list}")
    s3_paginator = boto3.client("s3").get_paginator("list_objects_v2")
    for item in s3_bucket_keys(
        S3_BUCKET,
        s3_paginator,
        "/",
        (".json"),
        delimiter="/",
        start_after="",
    ):
        if not item.startswith("consolidated"):
            # Append the item key to the list
            try:
                i_id = item.split("/")[0]
            except:
                i_id = item
            consolidated_keys_list.append({"i_id": i_id})
    LOGGER.debug(f"Writing = {consolidated_keys_list_object_name}")
    write_s3_object_content(
        S3_BUCKET, consolidated_keys_list_object_name, consolidated_keys_list
    )
    # Convert the json object to csv format
    # json_string = json.dumps(consolidated_list)
    # TODO parameterise the URL
    URL = f"{API_URL}/format-convert/v1/json-to-csv"
    request_payload = {"json": consolidated_keys_list}
    resp = requests.post(URL, json=request_payload)
    if resp.status_code == 200:
        resp_data_text = resp.text
        # Write the consolidated file (csv format)
        # LOGGER.debug(
        #     f"Converted to csv. Writing to {consolidated_keys_csvlist_object_name}"
        # )
        # LOGGER.debug(f"resp_data_text = {resp_data_text}")
        write_s3_object_content(
            S3_BUCKET, consolidated_keys_csvlist_object_name, resp_data_text, "csv"
        )
    else:
        LOGGER.debug(f"resp.status_code = {resp.status_code}. resp.text = {resp.text}")
    # Get items keys for all objects ending in .json modified after the date last processed
    new_items = False
    for item in s3_bucket_keys(
        S3_BUCKET,
        s3_paginator,
        "/",
        (".json"),
        delimiter="/",
        start_after="",
        last_modified_after=date_last_processed,
    ):
        LOGGER.debug(f"item = {item} and type = {type(item)}")
        # Exclude anything in the consolidated directory
        if not item.startswith("consolidated"):
            # Append the item key to the list
            new_items = True
            LOGGER.debug(f"processing file object = {item}")
            # Retrieve the object
            object_to_append = get_s3_object_content(S3_BUCKET, item)
            # LOGGER.debug(f"object_to_append = {object_to_append}")
            # Add the object to the existing json array
            consolidated_list.append(object_to_append)
    LOGGER.debug(f"consolidated_list = {consolidated_list}")
    if new_items:
        # Write the consolidated file (json)
        LOGGER.debug(f"Writing = {consolidated_list_object_name}")
        write_s3_object_content(
            S3_BUCKET, consolidated_list_object_name, consolidated_list
        )
        # Convert the json object to csv format
        # json_string = json.dumps(consolidated_list)
        request_payload = {"json": consolidated_list}
        resp = requests.post(URL, json=request_payload)
        if resp.status_code == 200:
            resp_data_text = resp.text
            # Write the consolidated file (csv format)
            # LOGGER.debug(
            #     f"Converted to csv. Writing to {consolidated_csvlist_object_name}"
            # )
            # LOGGER.debug(f"resp_data_text = {resp_data_text}")
            write_s3_object_content(
                S3_BUCKET, consolidated_csvlist_object_name, resp_data_text, "csv"
            )
        else:
            LOGGER.debug(
                f"resp.status_code = {resp.status_code}. resp.text = {resp.text}"
            )
    # Update the processed date
    processed_object["date_last_processed"] = current_date
    LOGGER.debug(f"Finishing up. Writing = {processed_object}")
    write_s3_object_content(S3_BUCKET, processed_object_name, processed_object)

    return


def find_s3_object(BUCKET_NAME, object_name):
    s3 = boto3.client("s3")
    LOGGER.debug(f"entering find_s3_object with {object_name}")
    try:
        file_exists = s3.head_object(Bucket=BUCKET_NAME, Key=object_name)
        last_modified = file_exists.get("LastModified")
    except botocore.exceptions.ClientError as e:
        LOGGER.warning(
            f"Error {e}. Object_name {object_name} not found in the bucket {BUCKET_NAME}"
        )
        LOGGER.warning("find_s3_object returning False")
        return False, None
    LOGGER.warning("find_s3_object returning True")
    return True, last_modified


def write_s3_object(WEBHOOK_BUCKET_NAME, object_name):
    s3 = boto3.client("s3")
    s3_resource = boto3.resource("s3")
    # Write the s3 file
    s3object = s3_resource.Object(WEBHOOK_BUCKET_NAME, object_name)
    response = s3object.put()
    LOGGER.debug(f"write_s3_object response = {response}")
    return


def delete_s3_object(WEBHOOK_BUCKET_NAME, object_name):
    s3 = boto3.client("s3")
    s3_resource = boto3.resource("s3")
    # Write the s3 file
    s3object = s3_resource.Object(WEBHOOK_BUCKET_NAME, object_name)
    response = s3object.delete()
    LOGGER.debug(f"delete_s3_object response = {response}")
    return


def get_s3_object_content(BUCKET_NAME, object_name):
    s3 = boto3.client("s3")
    LOGGER.debug(f"entering get_s3_object_content with {object_name}")
    try:
        file_exists = s3.head_object(Bucket=BUCKET_NAME, Key=object_name)
        if file_exists["ContentLength"] == 0:
            LOGGER.warning(object_name + " is empty")
            return "EMPTY"
        else:
            content_object = s3.get_object(Bucket=BUCKET_NAME, Key=object_name)
            file_content = content_object["Body"].read().decode("utf-8")
            file_object = json.loads(file_content)
            # LOGGER.debug(f"file_object = {file_object}")
            return file_object

    except botocore.exceptions.ClientError as e:
        LOGGER.warning(
            f"Error {e}. Object_name {object_name} not found in the bucket {BUCKET_NAME}"
        )
        return "NOT_FOUND"


def write_s3_object_content(
    WEBHOOK_BUCKET_NAME, object_name, file_object, file_format="json"
):
    s3 = boto3.client("s3")

    s3_resource = boto3.resource("s3")
    # Write the s3 file
    s3object = s3_resource.Object(WEBHOOK_BUCKET_NAME, object_name)
    if file_format == "json":
        s3object.put(Body=(bytes(json.dumps(file_object).encode("UTF-8"))))
    else:
        # LOGGER.debug(f"Writing {file_object} in format {file_format}")
        s3object.put(Body=file_object)

    return


def load_parameters():
    S3_BUCKET = os.environ["S3_BUCKET"]
    LOGGER.debug(f"S3_BUCKET = {S3_BUCKET}")

    return S3_BUCKET


def list_bucket_objects(bucket):
    s3 = boto3.resource("s3")
    s3_bucket = s3.Bucket(bucket)
    s3_bucket_objects = s3_bucket.objects.all()
    return s3_bucket_objects


def s3_bucket_keys(
    bucket_name,
    s3_paginator,
    prefix=None,
    suffixes=None,
    delimiter=None,
    start_after=None,
    last_modified_after=CONSOLIDATE_START,
):
    if isinstance(last_modified_after, str):
        last_modified_after = datetime.fromisoformat(last_modified_after)
    LOGGER.debug(
        f"last_modified_after = {last_modified_after}. Type = {type({last_modified_after})}"
    )
    prefix = prefix[1:] if prefix.startswith(delimiter) else prefix
    start_after = (start_after or prefix) if prefix.endswith(delimiter) else start_after
    if not suffixes:
        suffixes = ("",)
    else:
        suffixes = tuple(set(suffixes))
    for page in s3_paginator.paginate(
        Bucket=bucket_name, Prefix=prefix, StartAfter=start_after
    ):
        for content in page.get("Contents", ()):
            # LOGGER.debug(
            #     f"LastModified for item {content.get('Key')} = {content.get('LastModified')} Type = {type(content.get('LastModified'))}."
            # )
            # LOGGER.debug(
            #     f"Date check is {content.get('LastModified') > last_modified_after}"
            # )
            if (
                content.get("Key").endswith(suffixes)
                and content.get("LastModified") > last_modified_after
            ):
                yield content["Key"]
