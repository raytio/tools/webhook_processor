// @ts-check
"use strict";

const raytio = require("@raytio/decrypt-helper");
const { getFromBucket } = require("./getFileFromBucket");

/**
 * This lambda function responses to S3 events, not
 * conventional HTTP events.
 * @param {import("aws-lambda").S3Event} event
 */
module.exports.handler = async (event) => {
  try {
    const { a_id, i_id } = await getFromBucket({
      Key: event.Records[0].s3.object.key,
      Bucket: event.Records[0].s3.bucket.name,
    });

    if (!a_id) throw new Error("No a_id supplied");
    if (!i_id) throw new Error("No i_id supplied");

    await raytio
      .processSubmission({
        instanceId: i_id,
        applicationId: a_id,
        config: raytio.getAndValidateConfig(),
        verbose: true,
      })
      .then(raytio.generatePDF())
      .then(raytio.saveToS3Bucket());

    return {
      body: JSON.stringify({ ok: true }),
    };
  } catch (error) {
    console.error("\n\nwebhook_processor failed to handle a request\n\n");
    console.error(error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: `${error}` }),
    };
  }
};
