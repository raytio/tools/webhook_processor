// @ts-check
const AWS = require("aws-sdk");

const { AWS_REGION } = process.env;
const FALLBACK_REGION = "us-east-1";

if (!AWS_REGION) {
  console.log(
    "[warning] AWS_REGION not configured.",
    FALLBACK_REGION,
    "will be used."
  );
}

AWS.config.update({ region: AWS_REGION || FALLBACK_REGION });

/**
 * Fetches a JSON file from an S3 bucket and returns the result
 * @param {AWS.S3.Types.GetObjectRequest} options
 * @returns {Promise<Record<string, any>>}
 */
async function getFromBucket(options) {
  return new Promise((resolve, reject) => {
    const s3 = new AWS.S3();

    s3.getObject(options, (err, data) => {
      if (err) return reject(err);
      if (!data.Body) return reject(new Error("No data Body"));
      resolve(JSON.parse(data.Body.toString()));
    });
  });
}

module.exports = { getFromBucket };
