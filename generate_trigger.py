from __future__ import print_function
import requests
import boto3
import botocore.exceptions
import base64
import os
import json
import logging
logger = logging.getLogger('raytio.generate-submission')
logger.setLevel(logging.DEBUG)


def load_defaults():
    S3_BUCKET = os.environ['S3_BUCKET']
    logger.debug(f"S3_BUCKET = {S3_BUCKET}")

    return S3_BUCKET

def create_trigger_file(S3_BUCKET, trigger_contents):
    try:
        logger.debug(f"trigger_contents = {trigger_contents} and type = {type(trigger_contents)}")
        s3 = boto3.resource('s3')
        i_id = trigger_contents.get('i_id')
        if i_id is None:
            print(f"i_id = {i_id}")
            return
        file_name = f"{i_id}.trigger"
        s3object = s3.Object(S3_BUCKET, file_name)
        s3object.put(
                Body=(bytes(json.dumps(trigger_contents).encode('UTF-8')))
            )
        return trigger_contents
    except Exception as ve:
            print(f"Exception = {ve}")
            return

def lambda_handler(event, context):
    try:
        logger.debug(f"event = {event}")

        S3_BUCKET = load_defaults()

        body = json.loads(event['body'])

        response = create_trigger_file(S3_BUCKET, body)
        logger.debug(f"response = {response}")
        return ({'statusCode': 200,
                'isBase64Encoded': False,
                'headers': {'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Credentials': True
                            },
                'body': json.dumps(response)
                })

    except Exception as ve:
        print(f"Exception = {ve}")

