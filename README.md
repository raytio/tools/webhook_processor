# Raytio Webhook Processor

A template for using [`@raytio/decrypt-helper`](https://npm.im/@raytio/decrypt-helper) in an AWS Lambda function with the [serverless framework](https://serverless.com).

This project creates and deploys an application that will:

- receive a webhook from Raytio which contains the details of the application instance (submission) that has been created or updated by a user
- authenticate with the Raytio API,
- retrieve all of the data associated with that submission,
- decrypt the data,
- store it in `json`, `csv`, and `pdf` format in your S3 bucket in a folder named after the unique id of the submission (ther `i_id`) and
- periodically consolidate all of the individual submissions into a single file under the `consolidated` directory - one in `json` format and one in `csv` format.

Any binary data (e.g. images, videos, documents and other files) will also be stored in the bucket and referenced from within the stored data file. All of this happens within your AWS account.

## Getting Started

### What you need

1. An Amazon AWS account
2. A computer with the pre-requisites installed. The easiest way to do this is to create a machine using our AWS template (AMI) which will automatically create a computer that you can connect to from your PC running Windows, Linux or MacOS and then configure and deploy the webhook processor. Refer "Using our AWS remote desktop machine"

### Using our AWS remote desktop machine

We have created an Amazon Machine Instance ("AMI") which already has all of the pre-requisites installed and is ready to go. To set that up:

1. Login in to your [AWS account console](https://console.aws.amazon.com/console/home) and go to the [EC2 dashboard](https://console.aws.amazon.com/ec2) and select the [N. Virginia region](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:)
2. Choose "Launch Instances". Search for "Raytio" or "ami-0eeab9abe733a8fc3". Select the "Results in Community AMIs" links, and "Select" the AMI
3. In the "Choose an Instance Type" screen choose "t2.micro". Proceed to "Configure Instance Details"
4. In the "Configure Instance Details" screen ensure that "Auto-assign Public IP" is set to "Enable"
5. Proceed to "Add Storage"
6. Proceed to "Add Tags"
7. Proceed to "Configure Security Group"
8. Proceed to "Review and Launch"
9. Choose Launch
10. In the "Select an existing key pair or create a new key pair" choose "Proceed without a keypair" and tick the acknowledgment box. Select "Launch Instances"
11. Select "View instances", select the instance you just created and note the "Public IPv4 address". The username is `raytio` and password is `RAYrem0t3`
12. Connect to the machine using ssh and specify the IP noted above e.g. `ssh raytio@1.2.3.4`. For Windows users follow [these instructions](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse) to enable ssh
13. Now follow the instructions in the "Set up your AWS account with API access" section

Note: Don't forget to turn the machine off once you've finished the deployment otherwise you'll keep paying for it.

### Pre-requisites

If you want to build your own environment (and not use our AMI), then make sure you have the following pre-requisites installed:

- [nodejs](https://nodejs.org/en/download/)
- [yarn](https://classic.yarnpkg.com/lang/en/docs/install)
- [pip](https://pip.pypa.io/en/stable/installation/)
- [AWS cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
- [git](https://git-scm.com/downloads)

### Set up your AWS account with API access

1.  Go to the [AWS IAM console](https://console.aws.amazon.com/iam/home?region=us-east-1#/users)
2.  Select "Add User"
3.  Enter a username and tick "Programmatic access". Proceed to "Permissions"
4.  Select "Attach existing policies directly" and then tick the "AdministratorAccess" policy name. Proceed to "Tags"
5.  Proceed to "Review"
6.  Proceed to "Create User"
7.  Make a note of the "Access key ID" and "Secret access key". These will be used when configuring `aws` in the "Configure and deploy" section. Select "Close"

Now move on to the "Configure and Deploy" section

### Configure and deploy

Follow these steps either on the AWS remote desktop machine created above or else your own machine which has the pre-requisites installed:

1. Clone this repo
   ```sh
   git clone https://gitlab.com/raytio/tools/webhook_processor.git
   ```
2. Go into the new directory
   ```sh
   cd webhook_processor
   ```
3. Make a copy of the `config_template.yml` file and call it `config.yml`
   ```sh
   cp config_template.yml config.yml
   ```
4. Open the `config.yml` file using a text editor such as `nano` (e.g. `nano config.yml`). [Read these instructions](https://linuxize.com/post/how-to-use-nano-text-editor/) if you're not sure how to use Nano. Make the necessary parameter changes, save the file and exit Nano. At a minimum you will need to change the following under the `dev` section of the `config.yml` file:

   1. `S3_BUCKET`. Change `YOUR_BUCKET` to the name of a bucket where the files from the webhook processor will be saved. This must be unique across all of AWS so make sure it's a long name.
   2. `USERNAME`. Change `YOUR_USERNAME` to the username that you use to log in to your Raytio account
   3. `PASSWORD`. Change `YOUR_PASSWORD` to the password that you use to log in to your Raytio account
   4. `WEBHOOK_TO_CALL`. Change `EXTERNAL_WEBHOOK` to the URL that will be called when the shared data has been decrypted and stored in the S3 bucket.

5. Set up your AWS account with API access:
   1. Go to the [AWS IAM console](https://console.aws.amazon.com/iam/home?region=us-east-1#/users)
   2. Select "Add User"
   3. Enter a username and tick "Programmatic access". Proceed to "Permissions"
   4. Select the "Administrator_access" group. Proceed to "Tags"
   5. Proceed to "Review"
   6. Proceed to "Create User"
   7. Make a note of the "Access key ID" and "Secret access key". These will be used when configuring `aws` in the next step. Select "Close"
6. Configure `aws` with the relevant details including your AWS API keys
   ```sh
   aws configure
   ```
7. Activate the python virtual environment, install poetry and dependencies
   ```sh
   . env/bin/activate
   pip install poetry
   poetry install
   ```
8. Deploy the webhook processor to your AWS account. You should specify the "stage" as part of the deployment. There are three environments configured by default: `dev`, `staging`, `prod`. For example to create a development environment, specify `dev`
   ```sh
   yarn install
   yarn sls deploy --stage dev
   ```
9. If the deployment has finished successfully you should see a note

   ```sh
   Serverless: Stack update finished...
   ```

   Please take note of the endpoints that have been created. For example:

   ```sh
   endpoints:
      POST - https://n0tcp4s4l2.execute-api.us-east-1.amazonaws.com/dev/v1/process-submission
      PUT - https://n0tcp4s4l2.execute-api.us-east-1.amazonaws.com/dev/v1/process-submission
   ```

### What it does

The deployment will:

1. Create an S3 bucket where the output is stored. This S3 bucket is private to you and can only be accessed by users that you allow. Raytio cannot access this information.
2. Create a [Lambda function](https://aws.amazon.com/lambda/) to retrieve the user-submitted data from Raytio (using the Raytio API), decrypt the data and store it in the S3 bucket. Note that each submission will be stored in a folder with the name of the submission id. For a specific submission, there will be `json`, `csv` and `pdf` files created - these are named `{i_id}.json`, `{i_id}.csv` and `{i_id}.pdf` respectively. For example, given a bucket name of `raytio-submission-bucket` and a submission id (`i_id`) of `7d3869a8-f99a-4b16-8c4f-cad7860704f6` the `pdf` document can be retrieved at `https://raytio-submission-bucket.s3.amazonaws.com/7d3869a8-f99a-4b16-8c4f-cad7860704f6/7d3869a8-f99a-4b16-8c4f-cad7860704f6.pdf` In addition if the shared data includes binary files such as images or files, these will be stored in the bucket and folder named with the relevant profile object's `n_id`.
3. Create an [API Gateway](https://aws.amazon.com/api-gateway/) endpoint to listen for webhook requests from Raytio.
4. Create an [API Gateway](https://aws.amazon.com/api-gateway/) endpoint that can be passed the submission id (`i_id`) and an API key to retrieve the `json` object containing all of the decrypted data shared by the user.
5. (Optional) Create a [Lambda function](https://aws.amazon.com/lambda/) which sends a webhook request to a URL with the `i_id` of the submission that has been stored in the S3 bucket. Comment out the `sendWebhook` section in the `serverless.yml` file to disable this functionality.

### How to use it

1. Configure the state transitions on your [access application(s)](https://docs.rayt.io/docs/identityproof/data-receiver/create-access-application) with a type of `webhook` and the appropriate webhook destination from step 9 above e.g. https://n0tcp4s4l2.execute-api.us-east-1.amazonaws.com/dev/v1/process-submission.
2. Configure a webhook processor that will handle the `json` object containing the decrypted shared data that has been stored in the S3 bucket. For example if a user shares data with an `i_id` of `7d3869a8-f99a-4b16-8c4f-cad7860704f6` then a `POST` request will be sent to the url defined by the `WEBHOOK_TO_CALL` parameter in `config.yml`. The body of the request will be a `json` object containing the decrypted data.

## Developing

If developing locally, setup the parameters in `serverless.yml`. Then run:

```sh
yarn install
yarn start
```

You can then call the lambda function using:

```sh
curl -X POST http://localhost:4000/dev/api/v1/process-submission -d '{"a_id": "...","i_id":"..."}'
```

## Deploy

```sh
yarn sls deploy
```

This calls `sls deploy`
