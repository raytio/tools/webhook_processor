from __future__ import print_function
import requests
import boto3
import botocore.exceptions
import base64
import os
import json
import logging

logger = logging.getLogger("raytio.call-webhook")
logger.setLevel(logging.DEBUG)


def load_defaults():
    WEBHOOK_TO_CALL = os.environ["WEBHOOK_TO_CALL"]
    S3_BUCKET = os.environ["S3_BUCKET"]
    logger.debug(f"WEBHOOK_TO_CALL = {WEBHOOK_TO_CALL}. S3_BUCKET = {S3_BUCKET}")
    return WEBHOOK_TO_CALL, S3_BUCKET


def make_public(S3_BUCKET, key):
    s3_client = boto3.client("s3")
    file_name = f"{key}"
    try:
        logger.debug(f"Attempting to make public {file_name}")
        s3 = boto3.resource("s3")
        object_acl = s3.ObjectAcl(S3_BUCKET, key)
        response = object_acl.put(ACL="public-read")
        logger.debug(f"Set permissions on {file_name}")
    except botocore.exceptions.ClientError as e:
        logger.debug(f"Error setting permissions on {file_name}. {e}")
        return False


def send_webhook(WEBHOOK_TO_CALL, key):
    try:
        path_key = key.split(".")[0]
        i_id = path_key.split("/")[0]
    except Exception as e:
        raise Exception(f"Something broke {e}")
    # We shouldn't send a webhook if we've created a consolidated.json file
    if i_id == "consolidated":
        logger.debug(f"i_id == 'consolidated' so ignoring")
        return
    kvp = {}
    kvp["i_id"] = i_id
    data = json.dumps(kvp)
    resp = requests.post(WEBHOOK_TO_CALL, data=data)
    try:
        logger.debug(f"resp = {resp}")
        resp.raise_for_status()
        resp_data = resp.text
    except:
        raise Exception(f"Something broke {resp}")

    return resp_data


def delete_trigger_file(S3_BUCKET, key):
    s3_client = boto3.client("s3")
    file_name = f"{key}"
    try:
        try:
            logger.debug(f"Attempting to delete {file_name}")
            s3_client.delete_object(Bucket=S3_BUCKET, Key=file_name)
            logger.debug(f"Deleted {file_name}")
        except botocore.exceptions.ClientError as e:
            logger.debug(f"Error deleting {file_name}. {e}")
            return False

    except botocore.exceptions.ClientError as e:
        logger.debug(f"No file {file_name}. {e}")
        return False

    return True


def lambda_handler(event, context):
    try:
        logger.debug(f"event = {event}")

        WEBHOOK_TO_CALL, S3_BUCKET = load_defaults()

        # Iterate through records in event
        records = event.get("Records")
        if isinstance(records, list):
            for record in records:
                logger.debug(f"record = {record}")
                try:
                    key = record.get("s3").get("object").get("key")
                    logger.debug(f"key = {key}")
                    # make_public(S3_BUCKET, key)
                    send_webhook(WEBHOOK_TO_CALL, key)
                    # delete_trigger_file(S3_BUCKET, key)
                except Exception as e:
                    logger.debug(f"Something broke {e}")
                    raise Exception(f"Something broke {e}")
        else:
            logger.debug(f"records not a list {records}")
            raise Exception("records not a list")
        return

    except Exception as ve:
        print(f"Exception = {ve}")
