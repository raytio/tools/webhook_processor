from __future__ import print_function
import requests
import boto3
import botocore.exceptions
import base64
import os
import json
import logging
from uuid import UUID

logger = logging.getLogger("raytio.retrieve-submission")
logger.setLevel(logging.DEBUG)


def load_defaults():
    API_KEY = os.environ["API_KEY"]
    S3_BUCKET = os.environ["S3_BUCKET"]
    logger.debug(f"API_KEY = {API_KEY}. S3_BUCKET = {S3_BUCKET}")
    return API_KEY, S3_BUCKET


def is_valid_uuid(uuid_to_test, version=4):
    """
    Check if uuid_to_test is a valid UUID.

     Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

     Returns
    -------
    `True` if uuid_to_test is a valid UUID, otherwise `False`.

     Examples
    --------
    >>> is_valid_uuid('c9bf9e57-1685-4c89-bafb-ff5af830be8a')
    True
    >>> is_valid_uuid('c9bf9e58')
    False
    """

    try:
        uuid_obj = UUID(uuid_to_test, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test


def read_trigger_file(S3_BUCKET, key):
    s3 = boto3.client("s3")
    file_name = f"{key}/{key}.json"
    try:
        file_exists = s3.head_object(Bucket=S3_BUCKET, Key=file_name)
        if file_exists["ContentLength"] == 0:
            logger.warning(file_name + " is empty")
            return "Invalid i_id"
    except botocore.exceptions.ClientError as e:
        logger.warning(key + " file is not found in the bucket")
        logger.warning(f"error is {e}")
        return "Invalid i_id"

    content_object = s3.get_object(Bucket=S3_BUCKET, Key=file_name)
    file_content = content_object["Body"].read().decode("utf-8")
    json_content = json.loads(file_content)
    logger.debug(f"json_content = {json_content}")

    return json_content


def lambda_handler(event, context):
    try:
        logger.debug(f"event = {event}")

        API_KEY, S3_BUCKET = load_defaults()
        # Check API key
        query_parameters_dict = event["queryStringParameters"]
        logger.debug(f"query_parameters_dict = {query_parameters_dict}")
        error = None
        if query_parameters_dict is None:
            error = "Invalid API key"
        else:
            if query_parameters_dict.get("api-key") is not None:
                if query_parameters_dict.get("api-key") != API_KEY:
                    error = "Invalid API key"
            else:
                error = "Invalid API key"

        if error == "Invalid API key":
            return {
                "statusCode": 403,
                "isBase64Encoded": False,
                "headers": {
                    "Content-Type": "application/json",
                    "X-Amzn_ErrorType": "MissingHeaderException",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": True,
                },
                "body": json.dumps(
                    {"type": "Unauthorized", "message": "Invalid api-key provided."}
                ),
            }

        if event.get("pathParameters").get("i_id") is not None:
            i_id = event["pathParameters"]["i_id"]
            logger.debug(f"i_id = {i_id}")
            if not is_valid_uuid(i_id):
                logger.debug(f"Invalid i_id")
                error = "Invalid i_id"
            else:
                logger.debug(f"Valid i_id")
        else:
            error = "Invalid i_id"

        response = read_trigger_file(S3_BUCKET, i_id)
        logger.debug(f"response = {response}")
        if error == "Invalid i_id" or response == "Invalid i_id":
            return {
                "statusCode": 404,
                "isBase64Encoded": False,
                "headers": {
                    "Content-Type": "application/json",
                    "X-Amzn_ErrorType": "MissingHeaderException",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": True,
                },
                "body": json.dumps(
                    {"type": "Invalid i_id", "message": "The i_id provided is invalid."}
                ),
            }

        else:
            return {
                "statusCode": 200,
                "isBase64Encoded": False,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": True,
                },
                "body": json.dumps(response),
            }

    except Exception as ve:
        print(f"Exception = {ve}")
